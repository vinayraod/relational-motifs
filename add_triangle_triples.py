import json
import itertools
import sys

def get_triples():
	train2pgd = open('train2pgd.txt')

	triples = {}
	ent_set = set()

	for line in train2pgd:
		tokens = line.strip().split(' ')
		e0 = int(tokens[0])
		e1 = int(tokens[1])
		ent_set.add(e0)
		ent_set.add(e1)
		r = int(tokens[2])
		e0, e1 = sorted([e0, e1])
		triples[(e0, e1)] = triples.get((e0, e1), [])
		triples[(e0, e1)].append(r)

	print("No. of original triples: ", len(triples))
	return triples

def get_triangles():
	with open('triangles.json') as f:
		triangles = json.loads(f.readline())
	return triangles

def get_x2id(in_file):
	x2id = {}
	id2x = {}
	with open(in_file) as f:
		for line in f:
			tokens = line.strip().split('\t')
			x2id[tokens[0]] = int(tokens[1])
			id2x[int(tokens[1])] = tokens[0]

	return x2id, id2x

def add_triangles():
	count = len(ent2id)
	rel_tri_count = 0
	relation_triples = []
	new_entities = []
	new_triple_count = 0
	# Open the training file for appending new triples
	new_triples = open(sys.argv[3], 'a')
	for triangle in triangles:
		edges = list(itertools.combinations(triangle, 2))
		relations = []
		for edge in edges:
			edge = tuple([int(x) for x in edge])
			# print("Edge: ", edge)
			relations.append(triples[edge])
		# print(relations)
		rel_triples = list(itertools.product(*relations))
		rel_triples = [tuple(sorted(l)) for l in rel_triples]
		# print(rel_triples)
		rel_triples = list(set(rel_triples))
		for rel_trip in rel_triples:
			rels = [id2rel[rel_trip[x]] for x in range(3)]
			new_ent = '-'.join(rels)
			new_entities.append(new_ent)
			rel_triangle = 'relation-triangle-' + str(rel_tri_count)
			new_entities.append(rel_triangle)
			rel_tri_count += 1
			for rel in rels:
				new_triples.write('{}\t{}\t{}\n'.format(rel_triangle, new_ent, rel))
				new_triple_count += 1
	new_triples.close()
	# Append the new entities to the entity2id file
	with open(sys.argv[1], 'a') as f:
		# n = len(ent2id)
		for i, val in enumerate(new_entities):
			f.write('{}\t{}\n'.format(val, count+i))

	print("No. of entities added: ", len(new_entities))
	print("No. of triples added: ", new_triple_count)



if __name__ == '__main__':
	triples = get_triples()
	triangles = get_triangles()
	# entity2id file
	ent2id, id2ent = get_x2id(sys.argv[1])
	# relation2id file
	rel2id, id2rel = get_x2id(sys.argv[2])
	add_triangles()


