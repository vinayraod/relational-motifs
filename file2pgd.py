import sys

print("Converting the file to pgd format")

# entity2id file
e_file = open(sys.argv[1])

# relation2id file
r_file = open(sys.argv[2])

ent2id = {}
rel2id = {}

for line in e_file:
	tokens = line.strip().split('\t')
	ent2id[tokens[0]] = int(tokens[1])

for line in r_file:
	tokens = line.strip().split('\t')
	rel2id[tokens[0]] = int(tokens[1])

print("Number of entities: ", len(ent2id))
print("Number of relationships: ", len(rel2id))

# training file
t_file = open(sys.argv[3])

# Write out the converted file
with open('train2pgd.txt', 'w') as f:
	for line in t_file:
		tokens = line.strip().split('\t')
		f.write('{} {} {}\n'.format(ent2id[tokens[0]], ent2id[tokens[1]], rel2id[tokens[2]]))

print("Conversion done!!!")