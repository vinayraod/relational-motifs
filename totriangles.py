import sys
import json

print("Identifying all the triangles in PGD micro output")

# Open the file input to pgd
train2pgd = open(sys.argv[2])

# List to index entities set
entities = []
# Set of unique entities
ent_set = set()

# Read the lines and process it
for line in train2pgd:
	tokens = line.strip().split(' ')
	e0 = int(tokens[0])
	e1 = int(tokens[1])
	if e0 not in ent_set:
		entities.append(e0)
		ent_set.add(e0)
	if e1 not in ent_set:
		entities.append(e1)
		ent_set.add(e1)

# Open the output of pgd - micro file
in_file = open(sys.argv[1])

# Dict with key as sorted (e1, e2) and value as relationship no
edges_with_tri = {}

#Dict with key as node and value as list of adjacent nodes
adj_list = {}

# List to hold triangle tuples
triangles = []

in_file.readline()
for line in in_file:
	line = line.strip()
	if line.startswith('%') == True:
		continue
	tokens = line.split(',')
	ent1 = entities[int(tokens[0]) - 1]
	ent2 = entities[int(tokens[1]) - 1]
	edge = tuple(sorted([ent1, ent2]))
	val = int(tokens[2])
	if val != 0:
		edges_with_tri[edge] = val
		adj_list[ent1] = adj_list.get(ent1, [])
		adj_list[ent1].append(ent2)
		adj_list[ent2] = adj_list.get(ent2, [])
		adj_list[ent2].append(ent1)

print("No. of Edges with triangles: ", len(edges_with_tri))
print("Size of Adjacency list: ", len(adj_list))

print("Finding all the triangles....")

edges = edges_with_tri.keys()
for edge in edges:
	new_count = edges_with_tri[edge]
	if new_count == 0:
		continue
	adj0 = adj_list[edge[0]]
	adj1 = adj_list[edge[1]]
	adju = [val for val in adj0 if val in adj1]
	for node in adju:
		if new_count == 0:
			break
		tup1 = tuple(sorted([node, edge[0]]))
		tup2 = tuple(sorted([node, edge[1]]))
		if tup1 in edges_with_tri and tup2 in edges_with_tri:
			if edges_with_tri[tup1] != 0 and edges_with_tri[tup2] != 0:
				edges_with_tri[tup1] -= 1
				edges_with_tri[tup2] -= 1
		triangle = [node, edge[0], edge[1]]
		triangles.append(tuple(sorted(triangle)))
		new_count -= 1
	edges_with_tri[edge] = new_count


# print(set(triangles))
print("No. of unique triangles found: ", len(set(triangles)))
# print(triangles[0])
# print(triangles[1])
# print(set(triangles))

with open('triangles.json', 'w') as f:
	f.write(json.dumps(list(set(triangles))))