#!/bin/bash

pgd_path=$1
data_path=$2

ent="$data_path/entity2id.txt"
rel="$data_path/relation2id.txt"
train="$data_path/train.txt"

cp $ent "$data_path/entity2id-original.txt"
cp $rel "$data_path/relation2id-original.txt"
cp $train "$data_path/train-original.txt"

echo "Running file2pgd.py........."
python3.6 file2pgd.py $ent $rel $train
echo "Running PGD on the converted file.........."
"$pgd_path/pgd" -f train2pgd.txt --verbose --micro train2pgd.micro > pgd.out
echo "Done!!!"
echo "Running totriangles.py.........."
python3.6 totriangles.py train2pgd.micro train2pgd.txt
echo "Done!!!"
echo "Running add_triangle_triples.py........."
python3.6 add_triangle_triples.py $ent $rel $train
echo "Done!!!"

# ent=$1
# rel=$2
# train=$3 
# echo $1 $2 $3